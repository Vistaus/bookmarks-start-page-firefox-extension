// SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

export class Bookmarks {

	/**
	 * Get the name of a specific bookmark.
	 * @param {String} id ID of the bookmark.
	 * @returns {Promise<String>} Name of the bookmark.
	 */
	static async getName(id) {
		return (await browser.bookmarks.get(id))[0].title;
	}

	/**
	 * Get bookmarks from a specific folder.
	 * @param {String} id ID of the bookmark folder.
	 * @returns {Promise<Object[]>} An array containing the bookmarks.
	 */
	static async getFromFolder(id) {
		const bookmarks = await browser.bookmarks.getChildren(id);
		return bookmarks.filter(bookmark => bookmark.type === 'bookmark' && bookmark.url.match(/^(https?|file):\/\//));
	}

	/**
	 * Get folders from a specific folder.
	 * @param {String} id ID of the parent bookmark folder.
	 * @returns {Promise<Object[]>} An array containing the folders.
	 */
	static async getFolders(id) {
		const tree = id ? await browser.bookmarks.getSubTree(id) : await browser.bookmarks.getTree();
		return tree[0].children.filter(child => child.type === 'folder');
	}

}
