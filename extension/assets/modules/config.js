// SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

export class Config extends EventTarget {

	async getBookmarkFolderId() {
		return (await browser.storage.local.get('bookmarks-folder-id'))['bookmarks-folder-id'];
	}

	async setBookmarkFolderId(id) {
		await browser.storage.local.set({ 'bookmarks-folder-id': id });
		this.dispatchEvent(new CustomEvent('changed', { detail: 'id' }));
	}

}
