// SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { Bookmarks } from '../modules/bookmarks.js';
import { LinkList } from './linklist.js';
import { PrefsButton } from './prefsbutton.js';
import { PrefsPopup } from './prefspopup.js';

export class UI extends HTMLElement {

	constructor() {
		super();

		this.linkList = null;
		this.prefsButton = null;
		this.prefsPopup = null;

		this._build();
		this._buildUI();
		this._updatePageTitle();
		this._connect();
		this._style();
	}

	openPrefsPopup() {
		if ( this.linkList ) this.linkList.fade();
		this._buildPrefsPopup();
	}

	closePrefsPopup() {
		if ( !this.linkList ) return;
		this.linkList.unfade();
		this._removePrefsPopup();
	}

	_build() {
		this.attachShadow({ mode: 'open' });
		document.body.append(this);
	}

	async _buildUI() {
		if ( await self.config.getBookmarkFolderId() ) {
			this._buildLinkList();
			this._buildPrefsButton();
		} else {
			this._buildPrefsPopup();
		}
	}

	_removeUI() {
		this._removeLinkList();
		this._removePrefsButton();
		this._removePrefsPopup();
	}

	_buildLinkList() {
		this.linkList = new LinkList();
		this.shadowRoot.append(this.linkList);
	}

	_removeLinkList() {
		if ( !this.linkList ) return;
		this.shadowRoot.removeChild(this.linkList);
		this.linkList = null;
	}

	_buildPrefsButton() {
		this.prefsButton = new PrefsButton();
		this.shadowRoot.append(this.prefsButton);
	}

	_removePrefsButton() {
		if ( !this.prefsButton ) return;
		this.shadowRoot.removeChild(this.prefsButton);
		this.prefsButton = null;
	}

	_buildPrefsPopup() {
		this.prefsPopup = new PrefsPopup();
		this.shadowRoot.append(this.prefsPopup);
	}

	_removePrefsPopup() {
		if ( !this.prefsPopup ) return;
		this.shadowRoot.removeChild(this.prefsPopup);
		this.prefsPopup = null;
	}

	async _updatePageTitle() {
		const id = await self.config.getBookmarkFolderId();
		document.title = id ? await Bookmarks.getName(id) : browser.i18n.getMessage('extensionName');
	}

	_connect() {
		self.config.addEventListener('changed', () => {
			this._updatePageTitle();
			this._removeUI();
			this._buildUI();
		});
	}

	_style() {
		const style = document.createElement('style');
		style.textContent = `
:host
{
	--color: rgb(34, 34, 34);
	--background: rgb(249, 249, 249);
	--focus-background: rgb(255, 255, 255);
	--shadow-border: rgba(187, 187, 187, 0.2);
	--shadow-focus: rgba(187, 187, 187, 0.4);

	display: block;
	width: 100vw;
	height: 100vh;
	background: var(--background);
	color: var(--color);
	font: 1em/1em -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen-Sans, Ubuntu, Cantarell, 'Helvetica Neue', sans-serif;
}

@media (prefers-color-scheme: dark)
{
	:host
	{
		--color: rgb(204, 204, 204);
		--background: rgb(51, 51, 51);
		--focus-background: rgb(68, 68, 68);
		--shadow-border: rgba(0, 0, 0, 0.2);
		--shadow-focus: rgba(0, 0, 0, 0.4);
	}
}
		`;
		this.shadowRoot.append(style);
	}

}
